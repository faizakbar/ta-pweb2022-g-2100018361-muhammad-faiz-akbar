<html>
    <head><title> Tugas 10 </title></head>
    <body>
        <form method="post" action="">
            <b>PROGRAM KONVERSI NILAI</b><br><br>
            Masukkan nilai <input type="text" name="nilai">&emsp;
            <input type="submit" name="submit" value="Submit">
        </form>
    <?php
        $hasil = "<br><b>KONVERSI NILAI</b><br><br>";
        echo $hasil;
        if (isset($_POST['submit'])) {
            $nilai = $_POST['nilai'];
            echo "Nilai yang diinput : $nilai<br>";
            echo "Hasil konversi dari nilai angka ke nilai huruf sesuai dengan nilai PAP <br>";
            if ($nilai >= 80 && $nilai <= 100) {
                echo "Nilai anda A <br>";
                echo "Dengan nilai numerik 4.00";
            } 
            else if ($nilai >= 76.25 && $nilai <= 79.99) {
                echo "Nilai anda A- <br>";
                echo "Dengan nilai numerik 3.67";
            } 
            else if ($nilai >= 68.75 && $nilai <= 76.24) {
                echo "Nilai anda B+ <br>";
                echo "Dengan nilai numerik 3.33";
            } 
            else if ($nilai >= 65 && $nilai <= 68.74) {
                echo "Nilai anda B <br>";
                echo "Dengan nilai numerik 3.00";
            } 
            else if ($nilai >= 62.50 && $nilai <= 64.99) {
                echo "Nilai anda B- <br>";
                echo "Dengan nilai numerik 2.67";
            } 
            else if ($nilai >= 57.50 && $nilai <= 62.49) {
                echo "Nilai anda C+ <br>";
                echo "Dengan nilai numerik 2.33";
            }
            else if ($nilai >= 55.00 && $nilai <= 57.49) {
                echo "Nilai anda C <br>";
                echo "Dengan nilai numerik 2.00";
            } 
            else if ($nilai >= 51.25 && $nilai <= 54.99) {
                echo "Nilai anda C- <br>";
                echo "Dengan nilai numerik 1.67";
            } 
            else if ($nilai >= 43.75 && $nilai <= 51.24) {
                echo "Nilai anda D+ <br>";
                echo "Dengan nilai numerik 1.33";
            } 
            else if ($nilai >= 40 && $nilai <= 43.74) {
                echo "Nilai anda D <br>";
                echo "Dengan nilai numerik 1.00";
            } 
            else if ($nilai >= 0 && $nilai <= 39.99) {
                echo "Nilai anda C+ <br>";
                echo "Dengan nilai numerik 2.33";
            }  
            else {
                echo "Nilai anda tidak valid";
            }
        }
    ?>
    </body>
</html>