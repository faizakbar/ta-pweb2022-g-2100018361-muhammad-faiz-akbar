<?php 
    echo "<b>PROGRAM 9.1</b><br>";
    $gaji = 1000000;
    $pajak = 0.1;
    $thp = $gaji - ($gaji * $pajak);

    echo "Gaji sebelum pajak = Rp. $gaji <br>";
    echo "Gaji yang dibawa pulang = Rp. $thp <br>";

    echo"<br><br>";

    echo "<b>PROGRAM 9.2</b><br>";
    $a = 5;
    $b = 4;

    echo "Jika 1 maka true dan kosong maka false<br>";
    echo "$a == $b : ". ($a == $b);
    echo "<br>$a != $b : ". ($a != $b);
    echo "<br>$a > $b : ". ($a > $b);
    echo "<br>$a < $b : ". ($a < $b);
    echo "<br>($a == $b) && ($a > $b) : ". (($a != $b) && ($a > $b));
    echo "<br>($a == $b) || ($a > $b) : ". (($a != $b) || ($a > $b));
?>