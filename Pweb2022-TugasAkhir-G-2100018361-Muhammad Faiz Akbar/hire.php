<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="hire.css">
</head>
<body>
    <header>
        <section>
            <nav>
                <a href="#" class="logo">Faiz Akbar</a>
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="index.html">About</a></li>
                    <li><a href="index.html">Skills</a></li>
                    <li><a href="index.html">Contact</a></li>
                    <li><a href="hire.php">Hire Me</a></li>
                </ul>
            </nav>
        </section>
    </header>
    <main>
        <div class="container">
            <p class="p1" align="center"><b>HIRE ME</b></p>
            <form name="fform" method="POST" action="hire_submit.php">
                <div class="field">
                    <div class="col1">
                        <label for="fname">Nama</label>
                    </div>
                    <div class="col2">
                        <input type="text" name="name" id="name" placeholder="Nama"  >
                    </div>
                </div>
                <div class="field">
                    <div class="col1">
                        <label for="fcompany">Company</label>
                    </div>
                    <div class="col2">
                        <input type="text" name="Company" id="Company" placeholder="Company" >
                    </div>
                </div>
                <div class="field">
                    <div class="col1">
                        <label for="ftelp">Phone Number</label>
                    </div>
                    <div class="col2">
                        <input type="text" name="telp" id="telp" placeholder="Phone Number" >
                    </div>
                </div>
                <div class="field">
                    <div class="col1">
                        <label for="femail">Email</label>
                    </div>
                    <div class="col2">
                        <input type="email" name="email" id="demail"placeholder="Email" >
                    </div>
                </div>
                <div class="field">
                    <div class="col1">
                        <label for="fbudget">Budget</label>
                    </div>
                    <div class="col2">
                        <input type="text" name="budget" id="budget" placeholder="Budget ($)" >
                    </div>
                </div>
                <div class="field">
                    <div class="spasi"></div>
                    <input type="submit" value="Submit">
                </div>
            </form>
        </div>
    </main>
    <footer>
        <br><br>
        <p class="foot"><center>Copyright &copy; 2022 Muhammad Faiz Akbar</center></p>
    </footer>
</body>
</html>